For instructions on building and running the sample programs, please
visit the 'Using the Sample Programs on Windows' section in the Ice
3.5.0 release notes:

http://doc.zeroc.com/display/Rel/Ice+3.5.0+Release+Notes
