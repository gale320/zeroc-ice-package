This installer installs the following components:

 - Ice in /Library/Developer/Ice-@ver@
 - IceGrid Administrative Console in /Applications
 - Ice for Python in /Library/Python/2.7/site-packages

To uninstall Ice @ver@, please use the uninstall script that accompanies the distribution.
