Installing Ice @ver@

Double-click on Ice-@ver@.pkg and follow the instructions to install Ice on your computer.

Removing Ice @ver@

To remove Ice @ver@ from your computer, execute the uninstall.sh script as root in a Terminal:

  $ cd /Volumes/Ice-@ver@
  $ sudo ./uninstall.sh
   
uninstall.sh removes all Ice @ver@ installations from your computer.
