Ice 3.5.0 Third Party Packages
------------------------------

This distribution contains the development kit (including binaries)
for the open-source packages used by Ice 3.5.0 on Windows. Its primary
purpose is to simplify building Ice 3.5.0 from sources.

The following versions of the third party software were used to
create this package:

 - Berkeley 5.3.21
 - OpenSSL 1.0.1e
 - Expat 2.0.1
 - Bzip2 1.0.6
 - MCPP 2.7.2
 - JGoodies Forms 1.6.0
 - JGoodies Looks 2.5.2
 - JGoodies Common 1.4.0

This distribution provides:

 - Header files for Berkeley DB, OpenSSL, Expat and Bzip2 in the 
   include directory.

 - Import libraries for Berkeley DB, OpenSSL, Expat, Bzip2 and 
   MCPP built using Visual Studio 2010 SP1, in the lib and lib\x64 
   directories.

 - Import libraries for Berkeley DB, OpenSSL, Expat, Bzip2 and 
   MCPP built using Visual Studio 2012, in the lib\vc110 and 
   lib\vc110\x64 directories.

 - Import libraries for OpenSSL, Bzip2 and MCPP built using 
   Visual Studio 2008 SP1, in the lib directory.

 - Static library for MCPP built using MinGW 4.5.2, in the 
   lib\mingw directory.

 - DLLs and exes for Berkeley DB, OpenSSL, Expat and Bzip2 built
   using Visual Studio 2010 in the bin and bin\x64 directories.

 - DLLs for OpenSSL and Bzip2 built using Visual Studio 2008 SP1 
   in the bin directory.

 - DLLs and exes for Berkeley DB, OpenSSL, Expat and Bzip2 built
   using Visual Studio 2012 in the bin\vc110 and bin\vc110\x64 
   directories.

 - DLLS for Bzip2 and OpenSSL built using MinGW 4.5.2 in the
   bin directory.

 - JAR files for Berkeley DB, JGoodies Common, JGoodies Looks and
   JGoodies Forms in lib directory.
