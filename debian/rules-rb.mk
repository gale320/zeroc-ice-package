RB_PREFIX=$(CURDIR)/$(DEB_DH_INSTALL_SOURCEDIR)/usr
RB_RBDIR = $(CURDIR)/$(DEB_DH_INSTALL_SOURCEDIR)$(shell ruby1.8 -e \
	'require "rbconfig"; puts Config::expand("$$(rubylibdir)")')
RB_LIBDIR = $(CURDIR)/$(DEB_DH_INSTALL_SOURCEDIR)$(shell ruby1.8 -e \
	'require "rbconfig"; puts Config::expand("$$(archdir)")')

debian/stamp-build-rb:: debian/stamp-build-cpp
	@if test ! -f $@ ; then \
		$(MAKE_INVOKE) RUBY=ruby1.8 -C rb all; \
	fi
	:> $@

debian/stamp-install-rb:: MAKE_ARGS := \
	RUBY=ruby1.8 \
	prefix=$(RB_PREFIX) \
	install_rubydir=$(RB_RBDIR) \
	install_rblibdir=$(RB_LIBDIR) \
	install_slicedir=$(RB_PREFIX)/share/Ice-$(V)/slice
debian/stamp-install-rb:: debian/stamp-build-rb
	-mkdir -p $(RB_RBDIR) $(RB_LIBDIR)
	@if test ! -f $@ ; then \
		$(MAKE_INVOKE) $(MAKE_ARGS) -C rb install ; \
	fi
	:> $@

clean-rb::
	$(MAKE_INVOKE) -C rb clean
